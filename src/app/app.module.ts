import { LoaderSpinnerModule } from "./lib/loader-spinner/loader-spinner.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";

import { SlideManagerModule } from "./slide-manager/slide-manager.module";
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, SlideManagerModule, LoaderSpinnerModule],
  providers: [],

  bootstrap: [AppComponent]
})
export class AppModule {}
