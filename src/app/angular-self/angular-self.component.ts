import { User } from "./../slide-manager/User";
import { SlideService } from "./../slide-manager/slide.service";
import { Component, OnInit } from "@angular/core";

declare var Reveal: any;

@Component({
  selector: "app-angular-self",
  templateUrl: "./angular-self.component.html",
  styleUrls: ["./angular-self.component.scss"]
})
export class AngularSelfComponent implements OnInit {
  users: User[] = [];
  name: string;
  constructor(private slideService: SlideService) {}

  ngOnInit() {
    // this.getUsers();
  }

  public getUsers() {
    this.users = [];
    for (let i = 1; i <= 2; i++) {
      this.slideService.getAllUsers(i).subscribe(data => {
        const usersArray: User[] = data["data"];
        for (let j = 0; j < usersArray.length; j++) {
          this.users.push(usersArray[j]);
        }
      });
    }
  }
}
