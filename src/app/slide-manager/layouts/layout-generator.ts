import { AngularTestComponent } from "./../../angular-test/angular-test.component";
import { SlideService } from "./../slide.service";
import { ElementRef } from "@angular/core/src/linker/element_ref";
import { AngularSelfComponent } from "./../../angular-self/angular-self.component";
import { multi } from "./angular-charts/data";
import { Layout } from "./angular-layout/layout";
import { Data } from "./angular-table/data";
import { AngularLayoutComponent } from "./angular-layout/angular-layout.component";
import { AngularChartsComponent } from "./angular-charts/angular-charts.component";
import { AngularTableComponent } from "./angular-table/angular-table.component";
import {
  Injector,
  ComponentFactoryResolver,
  ApplicationRef,
  ComponentRef,
  Injectable
} from "@angular/core";

declare var Reveal: any;

@Injectable()
export class LayoutGenerator {
  private tableComponentRef: ComponentRef<AngularTableComponent>;
  private chartComponentRef: ComponentRef<AngularChartsComponent>;
  private selfComponentRef: ComponentRef<AngularSelfComponent>;
  private layoutComponentRef: ComponentRef<AngularLayoutComponent>;
  private testComponentRef: ComponentRef<AngularTestComponent>;

  constructor(
    private injector: Injector,
    private resolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private slideService: SlideService
  ) {}

  // Generating Charts Layout
  generateChartLayout(slide: any) {
    const chartCompFactory = this.resolver.resolveComponentFactory(
      AngularChartsComponent
    );
    this.chartComponentRef = chartCompFactory.create(this.injector);
    const instance = this.chartComponentRef.instance;

    Reveal.addEventListener(
      slide["view"],
      function() {
        // TODO: Sprinkle magic
        instance.getChartData();
      },
      false
    );

    this.appRef.attachView(this.chartComponentRef.hostView);
    this.chartComponentRef.onDestroy(() => {
      this.appRef.detachView(this.chartComponentRef.hostView);
    });

    return this.chartComponentRef.location.nativeElement;
  }

  // Generating Tabular Layout
  generateTableLayout(slide: any) {
    // creation component, AngularTableComponent should be declared in entryComponents
    const compFactory = this.resolver.resolveComponentFactory(
      AngularTableComponent
    );
    this.tableComponentRef = compFactory.create(this.injector);
    const instance = this.tableComponentRef.instance;

    Reveal.addEventListener(
      slide["view"],
      function() {
        // TODO: Sprinkle magic
        instance.getTableData();
      },
      false
    );

    // it's necessary for change detection within AppInfoWindowComponent
    this.appRef.attachView(this.tableComponentRef.hostView);
    this.tableComponentRef.onDestroy(() => {
      this.appRef.detachView(this.tableComponentRef.hostView);
    });

    return this.tableComponentRef.location.nativeElement;
  }

  // Generating Self Component Layout
  generateSelfLayout(slide: any) {
    return this.generateSelfComponentLayout(slide);
  }

  // Multiple Component layout
  multipleComponentLayout(components: Layout[]) {
    const layOutCompFactory = this.resolver.resolveComponentFactory(
      AngularLayoutComponent
    );
    this.layoutComponentRef = layOutCompFactory.create(this.injector);
    this.layoutComponentRef.instance.components = components;
    this.appRef.attachView(this.layoutComponentRef.hostView);
    this.layoutComponentRef.onDestroy(() => {
      this.appRef.detachView(this.layoutComponentRef.hostView);
    });

    return this.layoutComponentRef.location.nativeElement;
  }

  // Random Number Generator
  randomScalingFactor = function() {
    return Math.round(Math.random() * 100);
  };

  // Getting the exact self component
  generateSelfComponentLayout(slide: any) {
    const x = this.slideService.selfComponents;

    // for (let i = 0; i < x.length; i++) {
    //   const value = x[i];
    if (slide["componentName"] === "UserComponent") {
      const selfCompFactory = this.resolver.resolveComponentFactory(
        AngularSelfComponent
      );
      this.selfComponentRef = selfCompFactory.create(this.injector);
      const instance = this.selfComponentRef.instance;
      instance.name = slide["title"];

      Reveal.addEventListener(
        slide["view"],
        function() {
          // TODO: Sprinkle magic
          instance.getUsers();
        },
        false
      );

      this.appRef.attachView(this.selfComponentRef.hostView);
      this.selfComponentRef.onDestroy(() => {
        this.appRef.detachView(this.selfComponentRef.hostView);
      });

      return this.selfComponentRef.location.nativeElement;
    } else if (slide["componentName"] === "TestComponent") {
      const testCompFactory = this.resolver.resolveComponentFactory(
        AngularTestComponent
      );
      this.testComponentRef = testCompFactory.create(this.injector);
      const instance = this.testComponentRef.instance;
      instance.name = slide["title"];

      Reveal.addEventListener(
        slide["view"],
        function() {
          // TODO: Sprinkle magic
          // instance.getUsers();
        },
        false
      );

      this.appRef.attachView(this.testComponentRef.hostView);
      this.testComponentRef.onDestroy(() => {
        this.appRef.detachView(this.testComponentRef.hostView);
      });

      return this.testComponentRef.location.nativeElement;
    }
    // }
  }
}
