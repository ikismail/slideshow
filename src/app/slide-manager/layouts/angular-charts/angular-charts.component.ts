import { singleData } from "./data";
import { ChartConfig } from "./chart-config";
import { SlideService } from "./../../slide.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-angular-charts",
  templateUrl: "./angular-charts.component.html",
  styleUrls: ["./angular-charts.component.scss"]
})
export class AngularChartsComponent implements OnInit {
  name: string;

  data: any[] = [];

  view: any[] = [];

  colorScheme = {
    domain: ["#1d68fb", "#fa141b", "#33c0fc", "#fdbd2d"]
  };

  // options
  showXAxis: boolean;
  showYAxis: boolean;
  gradient: boolean;
  showLegend: boolean;
  showXAxisLabel: boolean;
  xAxisLabel: string;
  showYAxisLabel: boolean;
  yAxisLabel: string;
  autoScale = true;

  // pie
  showLabels: boolean;
  explodeSlices: boolean;
  doughnut: boolean;

  constructor(private slideService: SlideService) {}

  onSelect(event) {
    console.log(event);
  }

  ngOnInit() {
    // this.getChartData();
  }

  getChartData() {
    const x = this.slideService.getEvents();

    x.snapshotChanges().subscribe(slides => {
      slides.forEach(element => {
        const y = element.payload.toJSON() as any;

        if (y["view"] === "chart") {
          const configuration: ChartConfig = y["configuration"] as ChartConfig;

          this.doughnut = configuration.doughnut;
          this.explodeSlices = configuration.explodeSlices;
          this.gradient = configuration.gradient;
          this.showLabels = configuration.showLabels;
          this.showLegend = configuration.showLegend;

          const datas: any[] = [];

          Object.keys(y["data"]).forEach(function(key, data) {
            datas.push(y.data[key]);
          });

          this.data = datas;
          this.name = y["title"];
        }
      });
    });
  }
}
