export class ChartConfig {
  doughnut: boolean;
  explodeSlices: boolean;
  gradient: boolean;
  showLabels: boolean;
  showLegend: boolean;
  showXAxis: boolean;
  showyAxis: boolean;
  showXAxisLabel: boolean;
  showYAxisLabel: boolean;
  xAxisLabel: string;
  yAxisLabel: string;
}
