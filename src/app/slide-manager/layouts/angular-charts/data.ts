export const singleData = [
  {
    name: "Germany",
    value: 8940000
  },
  {
    name: "USA",
    value: 5000000
  },
  {
    name: "France",
    value: 7200000
  }
];

export const multi = [
  {
    name: "Germany",
    series: [
      {
        name: "17-02-2018",
        value: 6486
      },
      {
        name: "18-02-2018",
        value: 3014
      },
      {
        name: "19-02-2018",
        value: 3879
      }
    ]
  },

  {
    name: "USA",
    series: [
      {
        name: "17-02-2018",
        value: 4963
      },
      {
        name: "18-02-2018",
        value: 2176
      },
      {
        name: "19-02-2018",
        value: 5595
      },
      {
        name: "20-02-2018",
        value: 3864
      }
    ]
  },

  {
    name: "France",
    series: [
      {
        name: "17-02-2018",
        value: 2635
      },
      {
        name: "18-02-2018",
        value: 6294
      },
      {
        name: "19-02-2018",
        value: 4389
      }
    ]
  }
];
