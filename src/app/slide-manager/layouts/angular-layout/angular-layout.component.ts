import { Layout } from "./layout";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-angular-layout",
  templateUrl: "./angular-layout.component.html",
  styleUrls: ["./angular-layout.component.scss"]
})
export class AngularLayoutComponent implements OnInit {
  components: Layout[] = [];

  constructor() {}

  ngOnInit() {}
}
