import {
  Directive,
  Input,
  OnInit,
  Inject,
  Renderer2,
  ElementRef
} from "@angular/core";

@Directive({
  selector: "[appComponentRenderer]"
})
export class ComponentDirective implements OnInit {
  @Input() element;
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}
  ngOnInit() {
    this.renderer.appendChild(this.elementRef.nativeElement, this.element);
  }
}
