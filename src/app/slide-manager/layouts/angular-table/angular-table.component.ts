import { SlideService } from "./../../slide.service";
import { Data } from "./data";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-angular-table",
  templateUrl: "./angular-table.component.html",
  styleUrls: ["./angular-table.component.scss"]
})
export class AngularTableComponent implements OnInit {
  name: string;

  datas: any;

  constructor(private slideService: SlideService) {}

  ngOnInit() {
    this.getTableData();
  }

  getTableData() {
    this.datas = [];

    const x = this.slideService.getEvents();
    x.snapshotChanges().subscribe(slides => {
      slides.forEach(element => {
        const y = element.payload.toJSON() as any;
        if (y["view"] === "table") {
          const datas: Data[] = [];
          Object.keys(y["data"]).forEach(function(key, data) {
            datas.push(y.data[key]);
          });

          this.datas = datas;
          this.name = y["title"];
        }
      });
    });
  }
}
