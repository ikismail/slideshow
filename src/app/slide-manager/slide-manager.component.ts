import { Data } from "./layouts/angular-table/data";
import { Layout } from "./layouts/angular-layout/layout";
import { LayoutGenerator } from "./layouts/layout-generator";
import { LoaderSpinnerService } from "./../lib/loader-spinner/loader-spinner.service";
import { SlideService } from "./slide.service";
import { Component, OnInit } from "@angular/core";

declare var Reveal: any;
declare var Chart: any;

@Component({
  selector: "app-slide-manager",
  templateUrl: "./slide-manager.component.html",
  styleUrls: ["./slide-manager.component.scss"]
})
export class SlideManagerComponent implements OnInit {
  slides: any = [];

  datas: Data[] = [];

  color = Chart.helpers.color;

  constructor(
    private slideService: SlideService,
    private spinnerService: LoaderSpinnerService,
    private layoutGenerator: LayoutGenerator
  ) {}

  ngOnInit() {
    Reveal.initialize({
      controls: true,
      progress: true,
      history: true,
      center: true,
      transition: "slide", // default / none / slide / concave / convex / zoom
      transitionSpeed: "default", // default / fast / slow
      backgroundTransition: "default", // default / none / fade / slide / convex / concave/ zoom
      loop: true,
      autoSlide: 7000,
      width: "100%",
      height: "100%",
      margin: 0.1,
      minScale: 1,
      maxScale: 1
    });
    this.getAllSlides();
  }

  // Getting all slides from the database and
  getAllSlides() {
    this.spinnerService.show();
    const x = this.slideService.getEvents();
    this.slides = [];

    x.snapshotChanges().subscribe(slide => {
      // To reformat the slides
      const slideSection = document.getElementsByClassName("section");

      for (let i = 0; i < slideSection.length; i++) {
        while (slideSection[i]) {
          slideSection[i].parentNode.removeChild(slideSection[i]);
        }
      }
      // location.reload();
      this.spinnerService.hide();
      slide.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        // generating content for the slide

        if (y["view"] !== "chart") {
          const template = this.generateContent(y);
          const view = y["view"];
          this.createSlide(template, view);
        }
      });
      const multiContent = this.multipleComponent();
      this.createSlide(multiContent, 0);
    });
  }

  // Creating DOM Slides
  createSlide(template: any, view: any, index: any = -1) {
    const dom: any = {};
    dom.slides = document.querySelector(".reveal .slides");
    const newSlide = document.createElement("section");
    newSlide.className = "section";
    if (index === -1) {
      newSlide.classList.add("future");
      if (template["prefetching"].prefetch) {
        newSlide.setAttribute(
          "data-state",
          template["prefetching"].prefetchView
        );
      }
      dom.slides.appendChild(newSlide);
      document.querySelector(".navigate-right").classList.add("enabled");
    } else if (index > Reveal.getIndices().h) {
      newSlide.classList.add("future");
      dom.slides.insertBefore(
        newSlide,
        dom.slides.querySelectorAll("section:nth-child(" + (index + 1) + ")")[0]
      );
    } else if (index <= Reveal.getIndices().h) {
      newSlide.classList.add("past");
      dom.slides.insertBefore(
        newSlide,
        dom.slides.querySelectorAll("section:nth-child(" + (index + 1) + ")")[0]
      );
      Reveal.next();
    }
    if (template["contentType"] === "string") {
      newSlide.innerHTML = template["template"];
    } else {
      newSlide.appendChild(template["template"]);
    }
  }

  // Generating HTML Content for slides
  generateContent(slide: any) {
    let template = "";
    let contentType = "";
    let prefetching = {
      prefetch: false,
      prefetchView: String
    };

    if (slide["view"] === "table") {
      template = this.layoutGenerator.generateTableLayout(slide);
      contentType = "html";

      prefetching = {
        prefetch: slide["prefetch"].prefetch,
        prefetchView: slide["prefetch"].prefetchView
      };
    } else if (slide["view"] === "chart") {
      template = this.layoutGenerator.generateChartLayout(slide);
      contentType = "html";
      prefetching = {
        prefetch: slide["prefetch"].prefetch,
        prefetchView: slide["prefetch"].prefetchView
      };
    } else if (slide["view"] === "self") {
      template = this.layoutGenerator.generateSelfLayout(slide);
      contentType = "html";
      prefetching = {
        prefetch: slide["prefetch"].prefetch,
        prefetchView: slide["prefetch"].prefetchView
      };
    } else {
      template = "<h3>" + slide["title"] + "</h3>" + slide["template"];
      contentType = "string";
      prefetching = {
        prefetch: slide["prefetch"].prefetch,
        prefetchView: slide["prefetch"].prefetchView
      };
    }

    return {
      template: template,
      contentType: contentType,
      prefetching: prefetching
    };
  }

  multipleComponent() {
    let template = "";
    const prefetching = false;
    const contentType = "";
    const components: Layout[] = [];

    const x = this.slideService.getEvents();
    x.snapshotChanges().subscribe(slide => {
      this.slides = [];
      slide.forEach(element => {
        const layout: Layout = {
          component: "",
          col_size: 0,
          template: false,
          view: ""
        };

        const y = element.payload.toJSON();
        y["$key"] = element.key;
        const view = y["view"];
        if (view === "chart") {
          layout.component = this.layoutGenerator.generateChartLayout(y);
          layout.col_size = 6;
          layout.view = view;

          components.push(layout);
        }
        if (view === "self" && y["componentName"] === "UserComponent") {
          layout.component = this.layoutGenerator.generateSelfLayout(y);
          layout.col_size = 6;
          layout.view = view;

          components.push(layout);
        } else if (view === "default") {
          layout.template = true;
          layout.col_size = 6;
          layout.view = view;

          layout.component = y;
          components.push(layout);
        }
      });
    });

    template = this.layoutGenerator.multipleComponentLayout(components);
    return {
      template: template,
      contentType: "html",
      prefetching: prefetching
    };
  }
}
