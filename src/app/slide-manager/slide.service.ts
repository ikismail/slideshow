import { Injectable } from "@angular/core";
import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject
} from "angularfire2/database";
import { HttpClient } from "@angular/common/http/";

@Injectable()
export class SlideService {
  events: AngularFireList<any>;

  public selfComponents = ["UserComponent", "TestComponent"];

  private BASE_URL = "https://reqres.in/api";

  constructor(private db: AngularFireDatabase, private http: HttpClient) {}

  getEvents() {
    this.events = this.db.list("slides");
    return this.events;
  }

  getAllUsers(num: number) {
    return this.http.get(this.BASE_URL + "/users?page=" + num);
  }

  getSomeUnknownData(num: number) {
    return this.http.get(this.BASE_URL + "/unknown?page=" + num);
  }
}
