import { AngularTestComponent } from "./../angular-test/angular-test.component";
import { AngularSelfComponent } from "./../angular-self/angular-self.component";
import { LayoutGenerator } from "./layouts/layout-generator";
import { SlideService } from "./slide.service";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";

//  Third party Modules
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireModule } from "angularfire2";
import { FireBaseConfig } from "./../../environments/firebaseConfig";
import { ChartsModule } from "ng2-charts";

// Own Component
import { LoaderSpinnerModule } from "./../lib/loader-spinner/loader-spinner.module";
import { SlideManagerComponent } from "./slide-manager.component";
import { AngularTableComponent } from "./layouts/angular-table/angular-table.component";
import { AngularChartsComponent } from "./layouts/angular-charts/angular-charts.component";
import { AngularLayoutComponent } from "./layouts/angular-layout/angular-layout.component";
import { ComponentDirective } from "./layouts/angular-layout/component.directive";

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    AngularFireModule.initializeApp(FireBaseConfig),
    AngularFireDatabaseModule,
    LoaderSpinnerModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxChartsModule
  ],
  declarations: [
    ComponentDirective,
    SlideManagerComponent,
    AngularTableComponent,
    AngularChartsComponent,
    AngularSelfComponent,
    AngularLayoutComponent,
    AngularTestComponent
  ],
  entryComponents: [
    AngularTableComponent,
    AngularChartsComponent,
    AngularSelfComponent,
    AngularLayoutComponent,
    AngularTestComponent
  ],
  exports: [SlideManagerComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [SlideService, LayoutGenerator]
})
export class SlideManagerModule {}
