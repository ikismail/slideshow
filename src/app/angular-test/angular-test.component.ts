import { SlideService } from "./../slide-manager/slide.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-angular-test",
  templateUrl: "./angular-test.component.html",
  styleUrls: ["./angular-test.component.scss"]
})
export class AngularTestComponent implements OnInit {
  name: string;

  randomData: any = [];
  constructor(private slideService: SlideService) {}

  ngOnInit() {
    this.getSomeUnknownData();
  }

  public getSomeUnknownData() {
    this.randomData = [];
    for (let i = 0; i <= 2; i++) {
      this.slideService.getSomeUnknownData(i).subscribe(data => {
        const unknownData: any = data["data"];

        for (let j = 0; j < unknownData.length; j++) {
          this.randomData.push(unknownData[j]);
        }
      });
    }
  }
}
